#!/usr/bin/env bash

set -e
set -x
shopt -s dotglob

readonly name="GitSetup"
readonly ownership="GitSetup Upstream <kwrobot@kitware.com>"
readonly subtree="Utilities/GitSetup"
readonly repo="https://gitlab.kitware.com/utils/gitsetup.git"
readonly tag="setup"
readonly paths="
.gitattributes
git-gitlab-sync
setup-gitlab
setup-hooks
setup-lfs
setup-ssh
setup-upstream
setup-user
tips

LICENSE
NOTICE
README
"

extract_source () {
    git_archive
    echo "* -export-ignore" >> .gitattributes
}

. "${BASH_SOURCE%/*}/../thirdparty/update-common.sh"
