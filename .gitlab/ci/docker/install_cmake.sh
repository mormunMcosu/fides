#!/bin/sh

cd /tmp
mkdir -p Software

#install cmake
cd /tmp/Software
curl -OL https://github.com/Kitware/CMake/releases/download/v3.16.3/cmake-3.16.3-Linux-x86_64.tar.gz
tar xzf cmake-3.16.3-Linux-x86_64.tar.gz
mkdir -p /opt/cmake
mv cmake-3.16.3-Linux-x86_64/* /opt/cmake

cd /tmp
rm -rf Software/


