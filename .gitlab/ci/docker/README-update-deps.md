## Updating VTK-m and ADIOS2 version for CI

1. Update README.md with the new commit hash
2. Update `VTKM_HASH` or `ADIOS_HASH`, depending on which one you're updating, in `.gitlab/ci/docker/update-images.sh` (along with the date in the comment)
3. Run `./update-images.sh` from the .gitlab/ci/docker directory (note: your docker hub username needs authorization to push images to kitware/vtk)
