# known leak in adios remove when following issue is fixed:
# https://github.com/ornladios/ADIOS2/issues/3443
leak:adios2::format::BP5Deserializer::VarShape
