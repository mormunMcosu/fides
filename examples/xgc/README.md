## XGC Example

There are two examples for XGC in this directory.
`xgc.cxx` which reads all data from files and `xgc-sst.cxx`
which reads the mesh and some data from file, while reading the 3d data using SST.

### JSON details
This section describes any details needed in the JSON that are unique to running with XGC.

#### Coordinate System
For `array_type`, use `xgc_coordinates`.

#### Cell Set
`cells` should describe the array for the connectivity of the triangles (nd_connect_list).
`plane_connectivity` should describe the array used for the connectivity between adjacent planes (nextnode).

#### Fields
Some fields just save data for a single plane that is duplicated for all planes,
whereas some fields save data for each plane.
For these fields, the `array_type` should be set to `xgc_field`.

#### Number of Planes
Number of planes is needed by the cell set, coordinate system, and fields for XGC, 
so you will need to create a `number_of_planes` object in the JSON file.
Here is an example of how to specify the `number_of_planes`:

```
"number_of_planes": [
  {
    "source": "scalar",
    "data_source": "3d",
    "variable": "nphi",
    "planes_per_block": 4
  }
```

The source must be set to scalar. In this case, the data is stored in the nphi variable in the
data source named 3d.
There is an optional `planes_per_block` that can be set as well.
This determines how Fides handles blocks of XGC data.
The default value in Fides is 8 planes per block.
It is possible that each block has more than the set number of planes per block, if it does not
evenly divide the total number of planes.
In addition, each block has access to the first plane of the following block.
For example, with 4 planes and 2 planes per block, block 0 has access to planes 0, 1, and 2, 
while block 1 has access to planes 2, 3, and 0.
